import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;

public class Main {
    public static void main(String[] args) throws JRException {
        //Compilando jrxml a .jasper
        JasperCompileManager.compileReportToFile(
                "C:\\Users\\Hernan\\JaspersoftWorkspace\\MyReports\\BoardingPass.jrxml",
                "C:\\Users\\Hernan\\JaspersoftWorkspace\\MyReports\\BoardingPass.jasper");
        //Emulando datos vacios
        JREmptyDataSource dataSource = new JREmptyDataSource();
        JasperPrint jasperPrint = JasperFillManager.fillReport("C:\\Users\\Hernan\\JaspersoftWorkspace\\MyReports\\BoardingPass.jasper",null, dataSource);
        JRPdfExporter exp = new JRPdfExporter();
        exp.setExporterInput(new SimpleExporterInput(jasperPrint));
        exp.setExporterOutput(new SimpleOutputStreamExporterOutput("C:\\Users\\Hernan\\Desktop\\Everis\\Jasper\\BoardingPassReport.pdf"));
        SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
        exp.setConfiguration(conf);
        exp.exportReport();
        JasperPrint jasperPrintWindow = JasperFillManager.fillReport(
                "C:\\Users\\Hernan\\JaspersoftWorkspace\\MyReports\\BoardingPass.jasper", null, dataSource);
        JasperViewer jasperViewer = new JasperViewer(jasperPrintWindow);
        jasperViewer.setVisible(true);
    }
}
